﻿namespace CascadeDeleteTenant
{
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;

    class Program
    {
        static async Task Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                await db.Database.MigrateAsync();
            }
        }
    }
}
