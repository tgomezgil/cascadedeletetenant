# Introduction
This simple app is meant to test the possibility of using CASCADE DELETE for removing all the data related to a tenant from the database by just removing the tenant.

There are only 3 types of entities: Blogs, that contain Posts, all of them being linked to a Tenant.

Apparently the desired behaviour is not possible.

# Run it
Just run `dotnet run` command in a terminal **with admin privileges** (so it has access to the SQL server). You should see the following error:

```
Unhandled exception. Microsoft.Data.SqlClient.SqlException (0x80131904): Introducing FOREIGN KEY constraint 'FK_Posts_Tenants_TenantId' on table 'Posts' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.
```